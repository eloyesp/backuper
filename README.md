Backuper
========

Una configuración del tipo:

source: /home/eloyesp/documentos
dest:   /media/backup/documentos
diarios: 4
semanales: 3
mensuales: 2
anuales: 2

Un script que crea un backup incremental con rsync diario:

~~~ bash
#!/bin/bash

set -e

doc_src=/home/eloyesp/documentos

cd /media/backup/documentos

doc_dest=$(date +%F)
doc_last=$(ls | sort | tail -1)

if [ -d "$doc_dest" ]
then
  echo "backup already done"
  exit 1
fi

[ -d "$doc_last" ] && cp -al $doc_last $doc_dest

rsync --archive --delete $doc_src/ $doc_dest
~~~~

Por último, ese mismo script ejecuta keep_new.rb y borra todos los backups que
no coinciden con la configuración.

El resultado debería ser backups incrementales, ocupando poco espacio y
ordenados por fechas.
