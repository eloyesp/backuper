require 'date'
require 'set'

# hardcoded config
DAILY = 2
WEEKLY = 6
MONTHLY = 4

module Backuper

  module_function

  def group_by list, &block
    grouped = list.chunk(&block).map do |_, days|
      days.first
    end
  end

  def clean_backups backup_list
    backup_list = backup_list.sort

    # build a list for keeping
    keep = Set.new

    keep.merge backup_list.last(DAILY) # keep DAILY

    # keep WEEKLY
    by_week = group_by backup_list do |d|
      d.strftime '%G%U'
    end
    keep.merge by_week.last(WEEKLY)

    # keep MONTHLY
    by_month = group_by backup_list do |d|
      d.strftime '%Y%m'
    end
    keep.merge by_month.last(MONTHLY)

    keep.to_a.sort
  end
end
